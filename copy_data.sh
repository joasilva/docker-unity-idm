#!/bin/bash

#URL="https://b2drop.eudat.eu/index.php/s/GlqCx5X5BpHAwms/download"
DIST_URL="https://github.com/clarin-eric/unity-ldap/releases/download/1.9.6-C1.0.0/unity-server-distribution-1.9.6-dist.tar.gz"
DIST_VERSION="1.9.6"
DIR="${HOME}/.m2/repository/pl/edu/icm/unity/unity-server-distribution/${DIST_VERSION}"
FILE="unity-server-distribution-${DIST_VERSION}-dist.tar.gz"

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        #Remote / gitlab ci
        curl -L -o "${FILE}" "${DIST_URL}"
    else
        #Local copy
        cp "${DIR}/${FILE}" "${FILE}"
    fi
}

cleanup_data () {
    if [ -f "./image/${FILE}" ]; then
        rm  "./image/${FILE}"
    fi
}
