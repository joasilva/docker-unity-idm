#!/bin/bash

update_version_before () {
    echo "**** Updating version (v$1) in image/webContents/templates/default.ftl ****"
    sed -i "s/#VERSION#/v$1/g" webContents/templates/default.ftl
}

update_version_after () {
    echo "**** Reverting version (v$1) in image/webContents/templates/default.ftl ****"
    sed -i "s/v$1/#VERSION#/g" webContents/templates/default.ftl
}
