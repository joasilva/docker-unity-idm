#!/bin/bash
set -e

URL="https://shibboleth-sp/Shibboleth.sso/Metadata"
FILE="/data/metadata/sp-metadata.xml"

HOST=${_HOST:-"test"}

ADMIN_PASSWORD=${_ADMIN_PASSWORD:-"Admin12345"}
ENTITY_ID=${_ENTITY_ID:-"http:\/\/example-saml-idp.org"}
MAIN_PKI_KEY_ALIAS=${_PKI_KEY_ALIAS:-"uvos"}
MAIN_PKI_PASSWORD=${_PKI_PASSWORD:-"the!uvos"}

IDP_PKI_KEY_ALIAS=${_IDP_PKI_KEY_ALIAS:-"clarin-idp"}
IDP_PKI_PASSWORD=${_IDP_PKI_PASSWORD:-"the!uvos"}

SP_METADATA_URL=${_SP_METADATA_URL:-"file:\/\/\/data\/metadata\/sp-metadata.xml"}
MAIL_SMTP_SERVER=${_MAIL_SMTP_SERVER:-"smtp.transip.email"}
MAIL_SMTP_PORT=${_MAIL_SMTP_PORT:-"465"}
MAIL_SMTP_PASSWORD=${_MAIL_SMTP_PASSWORD:-"changethis"}
MAIL_SMTP_USERNAME=${_MAIL_SMTP_USERNAME:-"website@clarin.eu"}
MAIL_FROM=${_MAIL_FROM:-"accounts@clarin.eu"}

SERVER_CONF_FILE="/opt/unity-server/conf/unityServer.conf"
MAIL_CONF_FILE="/opt/unity-server/conf/mail.properties"
IDP_CONF_FILE="/opt/unity-server/conf/endpoints/saml-webidp.properties"
LDAP_CONF_FILE="/opt/unity-server/conf/endpoints/ldap.properties"
#INIT_FILE="/data/unity-server/conf/initialised"
PKI_FILE="/opt/unity-server/conf/pki.properties"

SSHD_DISABLED_FILE="/etc/supervisor/conf.d/sshd.conf.disabled"
SSHD_ENABLED_FILE="/etc/supervisor/conf.d/sshd.conf"

FLAG_DEBUG=0
FLAG_HELP=0
SKIP_METADATA=0



#
# Replace properties old value with new value
#
# Parameters:
#   property name
#   old value
#   new value
#   configuration file
#
function replace {
    echo "${1} in ${4}"
    sed -i "s/${1}=${2}/${1}=${3}/g" ${4}
}

function download_sp_metadata {
    echo "**************************"
    echo "Metadata Exchange"
    echo "**************************"

    if [ ! -f ${FILE} ]; then
        printf "Waiting for SP."
        until $(curl --output /dev/null --silent --head --fail --insecure ${URL}); do
            printf '.'
            sleep 1
        done
        printf " Done\n"

        printf "Downloading metadata."
        curl -o ${FILE}.tmp --insecure --silent ${URL}
        echo '<?xml version="1.0" encoding="UTF-8"?>' >> ${FILE}
        echo '<md:EntitiesDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:mdattr="urn:oasis:names:tc:SAML:metadata:attribute" xmlns:mdrpi="urn:oasis:names:tc:SAML:metadata:rpi" xmlns:mdui="urn:oasis:names:tc:SAML:metadata:ui" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:shibmd="urn:mace:shibboleth:metadata:1.0" xmlns:xrd="http://docs.oasis-open.org/ns/xri/xrd-1.0">' >> ${FILE}
        cat ${FILE}.tmp >> ${FILE}
        echo '</md:EntitiesDescriptor>' >> ${FILE}
        sed -i.bak "s/shibboleth-sp/${_SP_SERVERNAME}/g" ${FILE}
        printf ' Done\n'
    else
        echo "Metadata file [${FILE}] already exists"
    fi
}

#
# Update configuration and compile SASS theme if the INIT_FILE doesn't exist
#
function update_configuration {
    #if [ ! -f "${INIT_FILE}" ]; then
        echo "**************************"
        echo "Updating configuration"
        echo "**************************"
        #Main server config
        replace "unityServer.core.httpServer.advertisedHost" "192.168.59.109" "${HOST}" "${SERVER_CONF_FILE}"
        replace "unityServer.core.initialAdminPassword" "the!unity" "${ADMIN_PASSWORD}" "${SERVER_CONF_FILE}"
        #SAML IdP settings
        replace "unity.saml.issuerURI" "http:\/\/example-saml-idp.org" "${ENTITY_ID}" "${IDP_CONF_FILE}"
        replace "unity.saml.acceptedSPMetadataSource.1.url" "file:\/\/\/data\/metadata\/sp-metadata.xml" "${SP_METADATA_URL}" "${IDP_CONF_FILE}"
        #Update mail configuration
        replace "mail.from" "xxxx" "${MAIL_FROM}" "${MAIL_CONF_FILE}"
        replace "mail.smtp.host" "xxxx" "${MAIL_SMTP_SERVER}" "${MAIL_CONF_FILE}"
        replace "mail.smtp.port" "xxxx" "${MAIL_SMTP_PORT}" "${MAIL_CONF_FILE}"
        replace "mailx.smtp.auth.username" "xxxx" "${MAIL_SMTP_USERNAME}" "${MAIL_CONF_FILE}"
        replace "mailx.smtp.auth.password" "xxxx" "${MAIL_SMTP_PASSWORD}" "${MAIL_CONF_FILE}"

        #Update SSL configuration
        if [ -f "/opt/unity-server/conf/pki/clarin_cert.p12" ]; then
            replace "unity.pki.credentials.MAIN.path" "conf\/pki\/demoKeystore.p12" "conf\/pki\/clarin_cert.p12" "${PKI_FILE}"
            replace "unity.pki.credentials.MAIN.keyAlias" "uvos" ${MAIN_PKI_KEY_ALIAS} "${PKI_FILE}"
            replace "unity.pki.credentials.MAIN.password" "the!uvos" "${MAIN_PKI_PASSWORD}" "${PKI_FILE}"
        fi

         #Update IDP keypair configuration is a separate keystore is supplied
         #Otherwise the MAIN credential store is used to sign SAML assertions as well.
        if [ -f "/opt/unity-server/conf/pki/idpKeystore.p12" ]; then
            replace "unity.saml.credential" "MAIN" "IDP" "${IDP_CONF_FILE}"

            COUNT=$(grep "unity.pki.credentials.IDP.path=conf/pki/idpKeystore.p12" "${PKI_FILE}" | wc -l)
            if [ "${COUNT}" -eq 0 ]; then
                echo ""
                echo "unity.pki.credentials.IDP.format=pkcs12" >> "${PKI_FILE}"
                echo "unity.pki.credentials.IDP.path=conf/pki/idpKeystore.p12" >> "${PKI_FILE}"
                echo "unity.pki.credentials.IDP.keyAlias=${IDP_PKI_KEY_ALIAS}" >> "${PKI_FILE}"
                echo "unity.pki.credentials.IDP.password=${IDP_PKI_PASSWORD}" >> "${PKI_FILE}"
            else
                echo "IDP X.509 credential already configured"
            fi
        fi

        #Compiling SASS theme
        cd /opt/unity-server/bin &&
        /opt/unity-server/bin/unity-idm-scss-compile /opt/unity-server/webContents/VAADIN/themes/clarinTheme
        cd /opt/unity-server

        #Check sshd state depending on debug mode
        echo "Debug mode = ${FLAG_DEBUG}"
        if [ "${FLAG_DEBUG}" -eq 1 ]; then
            if [ -f "${SSHD_DISABLED_FILE}" ]; then
                echo "SSHD enabled"
                mv "${SSHD_DISABLED_FILE}" "${SSHD_ENABLED_FILE}"
            fi
        else
            if [ -f "${SSHD_ENABLED_FILE}" ]; then
                echo "SSHD disabled"
                mv "${SSHD_ENABLED_FILE}" "${SSHD_DISABLED_FILE}"
            fi
        fi

        echo "**************************"
        echo "Done"
        echo "**************************"
    #    echo "done" "${INIT_FILE}"
    #fi
}

function start_supervisor {
    /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
}

#Process command line arguments
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -d|--debug)
        FLAG_DEBUG=1
        ;;
    -s|--skip-metadata)
        SKIP_METADATA=1
        ;;
    -h|--help)
        FLAG_HELP=1
        ;;
    *)
        echo ""
        echo "Unkown option: $key"
        FLAG_HELP=1
        ;;
esac
shift # past argument or value
done


#Run entrypoint sequence
if [ "${FLAG_HELP}" -eq 1 ]; then
    echo ""
    echo "Supported arguments:"
    echo ""
    echo "  -d,--debug      Run in debug mode. This will start an ssh daemon so you can "
    echo "                  create an ssh tunnel to connect a java debugger as follows:"
    echo "                  ssh -f test@127.0.0.1 -L 4000:127.0.0.1:4000 -N -p 9000"
    echo ""
    echo "  -h,--help       Show this help message"
    echo ""
else
    if [ "${SKIP_METADATA}" -eq 0 ]; then
        download_sp_metadata
    else
        printf "Skipping metadata exchange\n"
    fi
    update_configuration
    start_supervisor
fi