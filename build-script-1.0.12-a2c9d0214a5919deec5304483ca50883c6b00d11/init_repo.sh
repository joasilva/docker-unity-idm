#!/bin/bash

set -e

LATEST_VERSION="1.0.11"

#Use version supplied in the 1st argument or a default value instead
VERSION=${1:-$LATEST_VERSION}

echo "Initializing directory with build-script v${VERSION}"

#TODO: update
#   take into account custom copy_data.sh. When this file is a symlink don't replace it
#TODO: verify md5sum
curl -s -L https://gitlab.com/CLARIN-ERIC/build-script/repository/archive.tar.gz?ref=${VERSION} | tar xz
ln -s build-script-${VERSION}-*/build.sh build.sh
ln -s build-script-${VERSION}-*/copy_data_noop.sh copy_data.sh
ln -s build-script-${VERSION}-*/update_version_noop.sh update_version.sh
chmod u+x build.sh
mkdir image run
touch image/.gitkeep
touch run/.gitkeep
git init
git add .
git commit -m "Intialized empty repo with build script v${VERSION}"

echo "Done"
