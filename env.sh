#!/bin/sh

if [ -n "$CI_SERVER" ]; then
    TAG="${CI_BUILD_TAG:-$CI_BUILD_REF}"
    IMAGE_QUALIFIED_NAME="${CI_REGISTRY_IMAGE}:${TAG}"
    IMAGE_FILE_NAME="${CI_REGISTRY_IMAGE##*/}:${TAG}"
else
    # WARNING: The current working dir must equal the project root dir.
    apk --quiet update --update-cache
    apk --quiet add 'git==2.8.3-r0'
    PROJECT_NAME="$(basename "$(pwd)")"
    TAG="$(git describe --always)"
    IMAGE_QUALIFIED_NAME="$PROJECT_NAME:${TAG:-latest}"
    IMAGE_FILE_NAME="${IMAGE_QUALIFIED_NAME}"
fi

IMAGE_FILE_PATH="$(readlink -fn 'output/')/$IMAGE_FILE.tar.gz"
export IMAGE_QUALIFIED_NAME
export IMAGE_FILE_PATH